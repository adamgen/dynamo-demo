const User = require('./userSchema');
const {performance} = require('perf_hooks');

const getUser = (email) => {
  return new Promise((resolve, reject) => {
    User.get(email, (err, user) => {
      if(err || !user) {
        reject(err);
      } else {
        performance.mark(email);
        performance.measure(email, 'start', email);
        const measure = performance.getEntriesByName(user.get('email'))[0];
        user.measure = measure;
        resolve(user);
      }
    });
  });
};

const getUsers = async (emails) => new Promise(async (resolve, reject) => {
  const usersPromises = [];
  performance.mark('start');
  emails.forEach(email => {
    const userPromise = getUser(email);
    usersPromises.push(userPromise);
  });

  // let rawUsers;
  Promise
    .all(usersPromises)
    .then(rawUsers => {
      if(!rawUsers) {
        console.log('rawUsers is ', rawUsers);
        reject();
      }

      const users = [];
      let usersSum = 0;
      let ageSum = 0;
      rawUsers.forEach(user => {
        const age = user.get('age');
        if(age) {
          ageSum += age;
          usersSum++;
        }
        users.push({
          name: user.get('name'),
          age,
          email: user.get('email'),
          duration: user.measure.duration,
        });
      });

      const ageAverage = ageSum / usersSum;
      resolve({users, ageAverage});
    })
    .catch(err => {
      reject(err);
      // console.log('userPromise.catch', err);
    });
});

module.exports = {
  getUsers,
  getUser,
};
