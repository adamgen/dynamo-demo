const User = require('./userSchema');
const usersList = require('./usersData');

const promise = new Promise((resolve, reject) => {
  User.create(usersList, function(err, acccounts) {
    if(err) {
      reject(err);
    } else {
      console.log('created 3 accounts in DynamoDB', acccounts);
      resolve();
    }
  });
});

module.exports = promise;