const dynamo = require('dynamodb');
const Joi = require('joi');
const User = dynamo.define('User', {
  hashKey: 'email',
  timestamps: true,
  schema: {
    email: Joi.string(),
    name: Joi.string(),
    age: Joi.number(),
  },
});

module.exports = User;