require('../initDBConnection');

const assert = require('assert');
const getUsers = require('../getUsers').getUsers;

describe('get users', function() {
  it('should get the correct number of users and average', function(done) {
    const emails = ['foo1@example.com', 'foo2@example.com', 'foo3@example.com'];
    getUsers(emails).then((data) => {
      assert.equal(data.ageAverage, 20);
      assert.equal(data.users.length, 3);
      done();
    }).catch(err => {
      assert(false);
      done();
    });
  });

  it('should reject when an email is not found', function(done) {
    const emails = ['foo1@example.com', 'foo2@example.com', 'foobar@example.com'];
    getUsers(emails).then((data) => {
      assert(false);
      done();
    }).catch(err => {
      assert(true);
      done();
    });
  });

  it('exclude from average count users that have no age field', function(done) {
    const emails = ['foo1@example.com', 'foo2@example.com', 'foo3@example.com', 'fooless@example.com'];
    getUsers(emails).then((data) => {
      assert.equal(data.ageAverage, 20);
      assert.equal(data.users.length, 4);
      assert(true);
      done();
    }).catch(err => {
      assert(false);
      done();
    });
  });
});
