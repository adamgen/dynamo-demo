# Dear reviewer

A precondition for it to work is you have dynamodb running on your local machine. If you have docker installed just run this `docker run -p 8000:8000 amazon/dynamodb-local`

A single command to clone and run tests `git clone https://gitlab.com/adamgen/dynamo-demo && cd dynamo-demo && npm i && node initData && npm run test`.

* I currently didn't include mockup data.
* To see the functioning tests we need to first create the db and add tables `node initData`
* There's an npm script for running the tests in watch mode `npm run test`
