module.exports = [
  {email: 'foo1@example.com', name: 'Foo 1', age: 10},
  {email: 'foo2@example.com', name: 'Foo 2', age: 20},
  {email: 'foo3@example.com', name: 'Foo 3', age: 30},
  {email: 'fooless@example.com', name: 'Foo 4'},
];