require('./userSchema');
const dynamo = require('dynamodb');

const promise = new Promise((resolve, reject)=>{
  dynamo.createTables(function(err) {
    if(err) {
      console.log('Error creating tables: ', err);
      reject(err);
    } else {
      console.log('Tables has been created');
      resolve();
    }
  });
});

module.exports = promise;