require('./initDBConnection');

Promise
  .all([
    require('./createTables'),
    require('./addUsers'),
  ])
  .then(() => {
    console.log('created data with success');
    process.exit();
  })
  .catch((err) => {
    console.log('faild creating data', err);
    process.exit();
  });
